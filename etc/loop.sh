#!/bin/bash

if [ "$#" -ne 3 ]
then
/bin/cat<<EOF
Usage: loop.sh FIRSTID LASTID TRESHOLD
Exiting....
EOF
    exit 1
fi

source /srv/python/pyejp/etc/setup.sh

counter=$1
LIMIT=$2
TRESHOLD=$3

while [ "$counter" -lt "$LIMIT" ]
do
  
  n_counter=`expr $counter + 1`
  echo "Executing pyejp --old $counter --new $n_counter --display --treshold $TRESHOLD"
  pyejp --old $counter --new $n_counter --display --treshold $TRESHOLD
  counter=$n_counter

done
exit 0
