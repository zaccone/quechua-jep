#!/bin/sh

QJEP_BIN='/srv/python/quechua-jep/bin'
export PATH=$QJEP_BIN:$PATH

QJEP='/srv/python/quechua-jep/src/'
export PYTHONPATH=$QJEP:$PYTHONPATH
