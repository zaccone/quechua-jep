import qjep.flow
import qjep.db.freqsets

class Jep(object):

    def __init__(self):
       self.prev_ht = None
       self.next_ht = None
       self.db = qjep.db.freqsets.Postgres()

    def run(self,old_oid,new_oid,treshold,updatedb,interval,tag,mrj):
        try:
            if not all ((old_oid,new_oid)):
                tmp_old_oid, tmp_new_oid = self.db.estimate_oids(interval,tag)
        except ValueError: # raised from sqlalchemy from estimate_oids()
            print "Cannot find matching operations, interval %s, tag %s" % (str(interval), str(tag))
            return None
        else:
            old_oid = old_oid if old_oid else tmp_old_oid 
            new_oid = new_oid if new_oid else tmp_new_oid

        self.oids = (old_oid, new_oid)

        self.prev_ht = self.__load(old_oid)
        self.next_ht = self.__load(new_oid)
        self.new_operation = self.__load_operation(new_oid)

        jeps = self.__get_jep()
        if treshold:
            c_jeps = self.__get_jep_w_treshol(treshold)
            jeps.extend(c_jeps)
        if updatedb:
            kwargs = {'mrj': bool(mrj),
                      'operation':self.new_operation
                     }

            args=list()
            self.db.update(jeps,*args,**kwargs)
        return jeps

    def __get_jep(self):
        """
        Get old and new keys, find keys difference.
        """
        prev_keys = set(self.prev_ht.keys())
        next_keys = set(self.next_ht.keys())

        diff_set = next_keys - prev_keys

        jeps = map(lambda x:self.next_ht[x],diff_set)
        return jeps

    def __get_jep_w_treshol(self,treshold):
        """
        Calculates the jeps basing on the counts
        """
        prev_keys = set(self.prev_ht.keys())
        next_keys = set(self.next_ht.keys())

        mutual_set = prev_keys & next_keys
        result = []
        for pk in mutual_set:
            p = self.prev_ht[pk]
            n = self.next_ht[pk]
            if self.__calculate_treshold(p.count, n.count, treshold):
                result.append(n)
        #print "DEBUG: count: %d, treshold jeps: %s" % (len(result),str(result))
        return result
            
    def __calculate_treshold(self,p,n,treshold):
        return float(n/p) >= float(1+treshold)

    def __load(self,oid):
        flows = self.db.fetch(oid)
        hashes = [ hash(x) for x in flows ]
        return dict(zip(hashes,flows))

    def __load_operation(self,oid):
        operation = self.db.operation(oid)
        return operation
       
