class Flow(object):

    IP_VER = dict( zip(('ipv4', 'ipv6'),(4,6)))
    PROTO = dict(zip(('tcp','udp'),(6,17)))

    def __init__(self):

        self._hash = None
        self.id = None
        self.oid = None # NEW
        self._ip_ver = None
        self._proto = 0

        self.src = '0.0.0.0'
        self.dst = '0.0.0.0'
        self.sport = 0
        self.dport = 0

        
    def __vals(self):
        l = [ self.ip_ver, self.proto,
              self.src, self.dst,
              self.sport, self.dport ]
        if not all(l):
            raise AttributeError("Some Flow attributes were not initialized")


    def __str__(self):
        return "%s: %s:%d -> %s:%d" % (self.proto,self.src,self.sport,self.dst,self.dport)

    def __repr__(self):
        return "%s: %s:%d -> %s:%d" % (self.proto,self.src,self.sport,self.dst,self.dport)


    def __hash__(self):
        """
        The most stupid hash function I have ever met.
        Need to fix it immediately
        """
        if not self._hash:
            h_str = ''.join( [ str(x) for x in (self.src, self.dst, self.sport, self.dport, self.proto) ] )
            self._hash = hash(h_str)
        return self._hash
        
    def __eq__(self,f):
        attrs = ('proto','src','dst','sport','dport')
        for attr in attrs:
            prop = getattr(self,attr)
            rprop = getattr(f,attr)
            if prop != rprop:
                return False
        else:
            return True


    @property
    def ip_ver(self):
        return self._ip_ver

    @ip_ver.setter
    def ip_ver(self,v):
        try:
            v = v.lower()
            self._ip_ver = Flow.IP_VER[v]
        except (AttributeError,KeyError):
            self._ip_ver = 0

    @property
    def proto(self):
        return self._proto

    @proto.setter
    def proto(self,p):
        try:
            p = p.lower()
            self._proto = Flow.PROTO[p]
        except (AttributeError,KeyError):
            self._proto = 0


    
