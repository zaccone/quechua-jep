import socket
import sqlalchemy

try:
    from sqlalchemy.ext.sqlsoup import SqlSoup
except ImportError:
    from sqlsoup import SQLSoup as SqlSoup

from sqlalchemy import or_, and_, desc

from qjep.flow import Flow


try:
    from qjep.db.settings import HOSTNAME 
    from qjep.db.settings import USER 
    from qjep.db.settings import PASSWORD 
    from qjep.db.settings import SCHEMA
    from qjep.db.settings import DB
except ImportError:
    print "Cannot import settings"

class Postgres(object):

    def __init__(self):
        credentials = 'postgresql://%s:%s@%s/%s' % (USER,PASSWORD,HOSTNAME,DB)
        self.engine = sqlalchemy.create_engine(credentials)
        self.db = SqlSoup(self.engine)
        self.db.schema = SCHEMA
        self.metadata = sqlalchemy.MetaData(schema=SCHEMA)

        self.connections_db = SqlSoup(self.engine)
        self.connections_db.schema = 'dionaea'

        self.HANDLERS = {}
        for fname in dir(self):
            if fname.startswith('handler'):
                _,name = fname.split('_',2)
                self.HANDLERS[name] = getattr(self,fname)

    def _handle(self,flow,*args,**kwargs):
        for arg in args:
            fn = self.HANDLERS.get(arg,lambda flow: flow)
            flow = fn(flow)
        for arg,val in kwargs.iteritems():
            fn = self.HANDLERS.get(arg,lambda flow,val: flow)
            print "flow: %s, val:%s, fn: %s" %(flow,val,fn)
            flow = fn(flow,val)
        return flow

    ## HANDLING FUNCTIONS
    @classmethod
    def handler_ejp(cls,flow,v):
        flow.ejp = v
        return flow
        
    @classmethod
    def handler_mrj(cls,flow):
        flow.interesting = any(flow.remote_host,flow.remote_port)
        return flow

    # TODO(marek): Think about making it a generator?
    def fetch(self,oid):
        """
        Fetches all the frequent itemsets and returns
        Flow objects
        """
        results = []
        fqs = self.__fetch__(oid)
        for fq in fqs:
            flow = Flow()
            flow.id = fq.id
            flow.oid = fq.oid # NEW
            flow.ip_ver = 'ipv4'
            flow.proto = fq.protocol
            flow.src = fq.remote_host
            flow.dst = fq.local_host
            flow.sport = fq.remote_port
            flow.dport = fq.local_port
            flow.count = fq.count

            results.append(flow)

        return results

    def operation(self,oid):
        operation = self.__fetch_operation__(oid)
        return operation

    def addr(self,s):
        """
        Converts string into IP address
        """
        if not s:
            return 0
        try:
            #return socket.inet_pton(socket.AF_INET,s)
            return s # JUST FOR NOW ON
        except socket.error,e:
            print e

    def update(self,flows,*args,**kwargs):
        """
        Update miner.freq_itemsets table, setting attribute 'ejp' to True for given
        list of flows
        """
        ids = [ flow.id for flow in flows ]
        for i in ids:
            flow = self.db.freq_itemsets.filter(self.db.freq_itemsets.id==i).one()
            flow.ejp = True
            flow.interesting = any((flow.remote_host,flow.remote_port))
            # PRZENIESC TO DO HANDLERA FLOWow.
            if flow.interesting:
                self._update_connections(flow,kwargs['operation'])
#            flow = self._handle(flow,*args,**kwargs)
        self.db.commit()

    def _update_connections(self,flow,operation):
        """
        Marks certain connections from miner.used_conns as interesting
        """
       
        query = ["""UPDATE miner.used_conns SET interesting = true WHERE generator = false AND connection IN (SELECT uc.connection FROM miner.used_conns uc JOIN
        dionaea.connections c ON uc.connection = c.connection WHERE uc.generator
        = false AND uc.oid = %d""" % (operation.id)]

        if flow.protocol is not None and flow.protocol != '':
            query.append("AND c.connection_transport = '%s'" % (flow.protocol,))
        if flow.remote_host is not None and flow.remote_host != '0.0.0.0':
            query.append("AND c.remote_host = '%s'" % (flow.remote_host,))
        if flow.remote_port is not None and flow.remote_port != 0:
            query.append("AND c.remote_port = %d" % (flow.remote_port,))
        if flow.local_host is not None and flow.local_host != '0.0.0.0':
            query.append("AND c.local_host = '%s'" %(flow.local_host,))
        if flow.local_port is not None and flow.local_port != 0:
            query.append("AND c.local_port = %d" % (flow.local_port,))

        query.append(')')    

        query = ' '.join(query)
        connections = self.connections_db.execute(query)

    # TODO(marek): Handle situation where no oids were found
    def estimate_oids(self,interval,tag):
        """
        Find lasts two operation ids and return them
        """
        if tag is not None:
            where = and_(self.db.operations.interval==interval,self.db.operations.tag==tag)
        else:
            where = and_(self.db.operations.interval==interval)

        new,old = self.db.operations.filter(where).\
                  order_by(self.db.operations.id.desc()).\
                  limit(2)
        return old.id, new.id

    def __fetch__(self,oid):
        where = and_(self.db.freq_itemsets.oid==oid,self.db.freq_itemsets.generator==False)
        freqs = self.db.freq_itemsets.filter(where).all()
        return freqs

    def __fetch_operation__(self,oid):
        where = and_(self.db.operations.id==oid)
        operation = self.db.operations.filter(where).one()
        return operation

